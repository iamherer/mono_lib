#pragma once

#include "pch.h"
#include "Clazz.h"

namespace mono
{

    struct Image
    {
        MonoImage *m_MonoImage;
        
        Image(MonoImage *monoImage) : m_MonoImage(monoImage)
        {
        }

        std::shared_ptr<Clazz> GetClazz(const std::string &nmsp, const std::string clzName)
        {
            return std::make_shared<Clazz>(mono_class_from_name(m_MonoImage, nmsp.c_str(), clzName.c_str()));
        }
    };
};