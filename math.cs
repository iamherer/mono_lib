using System;


public class Vec2
{
    public float x,y;
};

public class Vec3 
{
    public float x, y, z;
};


public class Vec4
{
    public float x,y,z,w;
};

public class Mat4x4
{
    Vec4 a,b,c ,d;
};

public class Mat3x3
{
    Vec3 a,b,c;
};
