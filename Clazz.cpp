#include "Clazz.h"
#include "Object.h"

using namespace mono;

std::shared_ptr<struct Object> Clazz::NewInstance(bool runtimeInvoke ) {

    return std::make_shared<Object>(mono_object_new(mono_domain_get(), m_Class) , runtimeInvoke);
}
MonoObject* mono::Clazz::NewInstanceObject()
{
    return mono_object_new(mono_domain_get(), m_Class);
}
;
