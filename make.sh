#!/bin/bash

foldername="build"

if [ -d "$foldername" ]; then
    echo "-- folder has exist"
else
    mkdir "$foldername"
    echo "-- create folder "
fi

rm hello.dll
# mcs -target:library -out:hello.dll hello.cs 
mcs -target:library -out:hello.dll math.cs hello.cs

cmake -B build && cd build && make && cd .. && ./build/main
