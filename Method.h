#pragma once
#include "pch.h"

namespace mono
{

    struct Method
    {
        MonoMethod *m_MonoMethod;
        Method(MonoMethod *monoMethod) : m_MonoMethod(monoMethod) {}
    };
};
