#pragma once

#include "Clazz.h"

namespace mono
{
    struct States
    {
        static std::unordered_map<std::string, std::shared_ptr<mono::Clazz>> m_ClazzMap;

        static void Register(const std::string &name, std::shared_ptr<mono::Clazz> clazz)
        {
            m_ClazzMap[name] = clazz;
        }
        static std::shared_ptr<mono::Clazz> GetClazz(const std::string &name)
        {
            return m_ClazzMap.at(name);
        }
    };

}