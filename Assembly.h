#pragma once

#include "pch.h"
#include "Image.h"

namespace mono
{

    struct Assembly
    {
        MonoAssembly *m_MonoAssembly;

        Assembly(MonoAssembly *inst) : m_MonoAssembly(inst)
        {
               };

        std::shared_ptr<Image> CreateImage()
        {
            return std::make_shared<Image>(mono_assembly_get_image(m_MonoAssembly));
        }
    };
};